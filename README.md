﻿# GTA San Andreas Launcher

Um laucher para contar as horas do GTA SA da Steam.

![print do launcher](https://i.imgur.com/C2x57JO.png)

### Por que você fez o seu se já existia um que faz a mesma coisa?

Porque dava erro de não encontrar o executável do MTA, então resolvi fazer o meu com a opção de alterar os caminhos dos executáveis pra evitar esse tipo de problema.

### Como faço para alterar os caminhos dos executáveis?

Abra o launcher e ele criará um arquivo na mesma pasta chamado `launcher_config.json` com os caminhos padrão, abra o arquivo no bloco de notas e edite como preferir. O arquivo padrão é esse:
```json
{
	"gta": "gta_sa.exe",
	"mta": "C:\Program Files (x86)\MTA San Andreas 1.5\Multi Theft Auto.exe",
	"samp": "samp.exe"
}
```