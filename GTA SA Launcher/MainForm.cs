﻿/*
 * Created by SharpDevelop.
 * User: Weslei Ramos
 * Date: 19/08/2019
 * Time: 20:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.IO;
using Commons.Json;


namespace GTA_SA_Launcher
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		const string arquivoPadrao = @"{
	""gta"": ""gta_sa.exe"",
	""mta"": ""C:\Program Files (x86)\MTA San Andreas 1.5\Multi Theft Auto.exe"",
	""samp"": ""samp.exe""
}";
		const string arquivoConfig = @"./launcher_config.json";
		private Config config;
			
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			config = new Config(JsonMapper.Parse(this.LerConfiguracoes()));
		}
		
		/*
		 * Abre um executável
		 */
		private void AbrirExecutavel(int n) {
			string caminho = config.gta;
			switch (n) {
				case 1:
					caminho = config.mta;
					break;
				case 2:
					caminho = config.samp;
					break;
			}
			
			if (File.Exists(caminho)) {
				Process.Start(caminho);
			} else {
				MessageBox.Show(String.Format("Não encontramos {0}", caminho), "Não foi possível encontrar o executável", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}
		
		/*
		 * Lê as configurações, caso não exista nenhuma cria um padrão
		 */
		private string LerConfiguracoes() {
			string content = arquivoPadrao;
			
			if (File.Exists(arquivoConfig)) {
				content = File.ReadAllText(arquivoConfig);
			} else {
				File.WriteAllText(arquivoConfig, content);
			}
			
			return content;
		}
		
		void OpenGTA(object sender, EventArgs e)
		{
			this.AbrirExecutavel(0);
		}
		
		void OpenMTA(object sender, EventArgs e)
		{
			this.AbrirExecutavel(1);
		}
		
		void OpenSAMP(object sender, EventArgs e)
		{
			this.AbrirExecutavel(2);
		}
		
		void OnLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start("https://gitlab.com/wesleiramos2/gta-san-andreas-launcher");
		}
	}
	
	class Config {
		public string gta = "";
		public string mta = "";
		public string samp = "";
		
		public Config(dynamic json) {
			gta = json.gta;
			mta = json.mta;
			samp = json.samp;
		}
	}
}
